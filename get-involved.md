---
layout: get-involved
title: Get Involved
konqi: assets/img/konqi-dev.png
sorted: 5
---

KDE Connect is a perfect project to start contributing to KDE. It is a
vast project that supports multiple platforms and includes several functionalities.

The KDE Connect community welcomes new contributors and you can join us in our
communication channels via Matrix (<a href="https://webchat.kde.org/#/room/#kdeconnect:kde.org">#kdeconnect:kde.org</a>), 
[Telegram](https://t.me/joinchat/AOS6gA37orb2dZCLhqbZjg) or the
KDE Connect [mailing list](https://mail.kde.org/mailman/listinfo/kdeconnect).

Feel free to ask them any question you may have!

## Development

You'll need a basic understanding of programming concepts, the rest can be
learned by doing. Experience with Android or Qt is beneficial, but not needed.

All patches are submitted on [Gitlab](https://invent.kde.org). Be sure to
select the most relevant template.
You don't need to assign any reviewers, the developers are subscribed to
notifications. Should this be your first patch, it's good to know that it
might take some time before your patch is reviewed (we all work on KDE Connect
in our free time), and you'll probably have to make some changes a couple of
times. That's not because you're new, that's what happens for all reviews
(even for long-time contributors).

<div class="d-flex flex-wrap justify-content-around">
  <a class="button mb-2" href="https://community.kde.org/KDEConnect#Setting_up_KDE_Connect_Repository_for_Development">
    Setting up a KDE Connect Repository for Development
  </a> 
  <a class="button mb-2" href="https://community.kde.org/KDEConnect#Development_tips">
    Development tips
  </a> 
</div>

### KDE Connect for Linux, Windows, MacOS and Linux Mobile 

The KDE Connect version for Linux, Windows, MacOS and Linux Mobile is written
in C++ and QML with the help of the [Kirigami framework](https://kde.org/products/kirigami).
[KF5PulseAudioQt](https://cgit.kde.org/pulseaudio-qt.git/) and
[KF5PeopleVCard](https://cgit.kde.org/kpeoplevcard.git/) are also needed.

The git repository for this version can be found [here](https://invent.kde.org/kde/kdeconnect-kde).

To build this version, you need a recent version of the [Qt Framework](https://qt.io) and
the [KDE Frameworks](https://kde.org/products/frameworks).

<div class="d-flex justify-content-around flex-wrap">
  <a class="button mb-2" href="https://community.kde.org/KDEConnect#Linux_Desktop">Build for Linux</a> 
  <a class="button mb-2" href="https://community.kde.org/KDEConnect/Build_Windows">Build for Windows</a> 
  <a class="button mb-2" href="https://community.kde.org/KDEConnect/Build_MacOS">Build for macOS</a> 
</div>

### KDE Connect for Android

The KDE Connect version for Android is written in Java against the standard
Android SDK.

The Android repository can be found [here](https://invent.kde.org/kde/kdeconnect-android).

The easiest way to build KDE Connect for Android manually is to use Android Studio to build the app and install it to your phone,
although it can be done on the command line using gradle. 

Running KDE Connect in an emulator is possible, but this requires an advanced
networking setup. See the guide posted for the
[Android_Emulator](https://community.kde.org/KDEConnect/Android_Emulator).

### KDE Connect for iOS

The KDE Connect version for iOS is written in Swift and the SwiftUI framework.

The iOS repository can be found [here](https://invent.kde.org/network/kdeconnect-ios).

Although building to physical iOS devices requires you to be a member of the K Desktop Environment e.V.'s App Store development team
(or you can [request](https://developer.apple.com/contact/request/networking-multicast) Apple to give your development team a
[Multicast Networking Entitlement](https://developer.apple.com/documentation/bundleresources/entitlements/com_apple_developer_networking_multicast)),
you should be able to build to and test the complete set of functionalities using iOS simulators.

For more, please checkout the [Contributing](https://invent.kde.org/network/kdeconnect-ios#contributing) section in README.

### Junior Job

There are a couple of tasks marked as [Junior Jobs](https://phabricator.kde.org/project/board/159/)
on our workboard. Those have some extra information on how to approach them
that can help you get started. 

## Not a Programmer?

Not a problem! There's plenty of other tasks that you can help us with to
make KDE Connect better, even if you don't know any programming languages!

* [Bug triaging](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging) - help us find
  misfiled, duplicated or invalid bug reports in Bugzilla
* [Localization](https://community.kde.org/Get_Involved/translation) - help translate
  KDE Connect into your language
* [Documentation](https://community.kde.org/Get_Involved/documentation) - help us improve our user
  documentation to make KDE Connect more friendly for newcomers
* [Promotion](https://community.kde.org/Get_Involved/promotion) - help us promote KDE Connect
  both online and offline
* [Updating the wiki](https://userbase.kde.org/KDEConnect) - help update the information present in
  the wiki, add new tutorials and improve content in general, making it easier for others to join!
* Do you have any other idea? Get in touch!

